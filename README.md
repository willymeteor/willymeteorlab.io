[![copyright][badge]][licence]

[![willy][image]][website]

[badge]: public/files/assets/badge.svg
[licence]: LICENSE.md
[image]: public/files/assets/velim.png
[website]: https://www.velicihan.com/
